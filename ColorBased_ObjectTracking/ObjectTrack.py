 # -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import sys
sys.path.append("../OpenCV_CameraTools")
import cv2
import traceback
import numpy as np
from CameraTools import CameraStream, Capture
###############################################################################
# Header
###############################################################################
__author__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']
###############################################################################
###############################################################################

def ObjectTracking(init_frame, color="red"):
    """
    Method to track objects with red, green or blue colors
    Parameters
        ----------
        init_frame : initial frame
        color : string with color (red, green or blue)
    """
    
    #Define range of colors in HSV for object tracking
    if color == "red":
        lower = np.array([100,100,120])
        upper = np.array([179,255,255])
    elif color == "green":
        lower = np.array([20,0,0])
        upper = np.array([80,120,120])
    elif color == "blue":
        lower = np.array([60,220,0])
        upper = np.array([255,255,255])
        
    # setup initial location of tracking window
    x,y,w,h = 600,200,250,250        
    track_window = (x,y,w,h)          
        
    #Convert init_frame to hsv and set up for tracking
    init_hsv = cv2.cvtColor(init_frame,cv2.COLOR_RGB2HSV)
    
    #Threshold the HSV image to get only red colors
    mask = cv2.inRange(init_hsv, lower, upper)
    roi_hist = cv2.calcHist([init_hsv],[2],None,[256],[0,256])
    cv2.normalize(roi_hist,roi_hist,0,255,cv2.NORM_MINMAX)
    
    #Setup the termination criteria, either 10 iteration or move by atleast 1 pt
    term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )
    
    #Start camera stream
    stream = CameraStream(channel=0,resolution='HD',colormode='bgr')
    stream.Start()
        
    while True:
        #Grab frames and show it
        frame = stream.GetFrame()  
        cv2.imshow('Color based Object Tracking',frame)  
        
        #Convert frame to hsv
        hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)           
        mask = cv2.inRange(hsv, lower, upper)
        
        # Bitwise-AND mask and original image
        res = cv2.bitwise_and(hsv, hsv, mask= mask)
        dst = cv2.calcBackProject([res],[2],roi_hist,[0,256],1)

        #Apply meanshift to get the new location
        ret, track_window = cv2.meanShift(dst, track_window, term_crit)

        #Draw it on image
        x,y,w,h = track_window
        cv2.rectangle(frame, (x,y), (x+w,y+h), 255, 1)          
        
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(frame,'X value of window = {x_val}'
                    .format(x_val=x),(30,40), font, 0.8,(128,0,0),1,1)
        cv2.putText(frame,'Y value of window = {y_val}'
                    .format(y_val=y),(30,70), font, 0.8,(128,0,0),1,1)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    stream.Stop()
    cv2.destroyAllWindows()               

###############################################################################        

if __name__ == "__main__":
    
    try:
        
        #Start Camera Stream and capture initial frame
        stream = CameraStream(channel=0,resolution='HD',colormode='bgr')
        cap = Capture(stream)
        init_frame = cap.Image(img_save=None, img_flip="no")
        
        #Start Object tracking
        ObjectTracking(init_frame, color="red")

    except:
        traceback.print_exc()
        