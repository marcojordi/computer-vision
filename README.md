# Computer Vision Projects
This repository contains various Computer Vision Projects, written in Python 3.6.xx using the computer vision and machine learning library [OpenCV](https://github.com/opencv).

- OpenCv Basic Camera Tools (Capture Image and video and display it)
- Color Based Object Tracking (red, green, blue)

