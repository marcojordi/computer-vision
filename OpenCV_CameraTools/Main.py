 # -*- coding: utf-8 -*-
###############################################################################
# Import section
###############################################################################
import traceback
from CameraTools import CameraStream, DisplayImage, Capture
###############################################################################
# Header
###############################################################################
__author__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']
###############################################################################
###############################################################################

if __name__ == "__main__":
    try:
        
        #Start Camera Stream and capture image
        Stream = CameraStream(channel=0,resolution='HD',colormode='bgr')
        cap = Capture(Stream)
        img1 = cap.Image(img_save=None, img_flip="no")
        
        #Display image
        DisplayImage(name="Image 1",frame=img1)
        
        #Start Camera Stream and capture video
        Stream = CameraStream(channel=0,resolution='HD',colormode='bgr')
        cap = Capture(Stream)
        cap.Video(vid_flip="no")        #Press "q" to stop video

    except:
        traceback.print_exc()
        