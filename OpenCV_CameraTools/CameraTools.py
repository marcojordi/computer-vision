# -*- coding: utf-8 -*-
###############################################################################
#
# Import section
###############################################################################
from __future__ import division
import cv2
import threading
import numpy as np
import matplotlib.pyplot as plt
###############################################################################
#
# Header
###############################################################################
__author__ = ['Marco Jordi', 'Benjamin Loeffel']
__maintainer__ = ['Marco Jordi']
__email__ = ['marco.jordi@bfh.ch']

"""
Created on Wed Nov  7 10:11:36 2018

@author: Marco Jordi
"""
###############################################################################
###############################################################################

class CameraStream:
    """
    CameraStream class to provide easy access for a camera
    """
    __ColorModeDict={'rgb':     cv2.COLOR_BGR2RGB,
                     'hsv':     cv2.COLOR_BGR2HSV,
                     'gray':    cv2.COLOR_BGR2GRAY}
                     
    __ResolutionDict={'FullHD': [1920,1080],
                     'HD':      [1280,720],
                     'EVGA':    [1042,768],
                     'VGA':     [640,480]}
                    
    def __init__(self, channel=0,resolution='HD',colormode='bgr'):
        """
        Constructor of CameraStream
        
        Parameters
        ----------
        channel : scalar, int, optional
            Videochannel number to use, default = 0
        resolution : string or tuple (widht,height)
            Preset values can be:
                'FullHD'
                'HD'
                'EVGA'
                'VGA'
                
        colormode : scalar, string, optional
            Colorspace of returned image,
            Values could be:
                'bgr',
                'rgb',
                'hsv',
                'gray'
        """
        self.stopped = False
        
        self.colormode=colormode
        
        if type(resolution)==tuple:
            print('Resolution manually set')
            self.width=resolution[0]
            self.height=resolution[1]
        else:
            print('Start CameraStream with resolution {preset}'.format(preset=resolution))
            self.width=self.__ResolutionDict[resolution][0]
            self.height=self.__ResolutionDict[resolution][1]
        
        self.stream = cv2.VideoCapture(channel)

        #########################
#        Setting the properties
        #########################        
#       Framesize
        if int(cv2.__version__.split('.')[0])==3:
            self.stream.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
            self.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height) 
        else:
            self.stream.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, self.width)
            self.stream.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, self.height)
            
        self.frame= self.__Read()
        
        size=np.shape(self.frame)
        if size[0] != self.height or size[1] != self.width:
            raise ValueError('The specified and the actual resolution missmatch, please verify that the camera can handle the desired resolution')
        
    def __Read(self):
        """
        Reads a frame from VideoCapture
        
        Returns
        -------
        frame : 2D/3D np.array, int
            Frame containing the image Information
        """
        (success,frame) = self.stream.read()
        
        if success == False:
            frame=np.uint8(np.zeros((self.height,self.width,3)))
            ErrorMessage='Error: No frame could have been read'
            cv2.putText(frame,ErrorMessage,(50,200), cv2.FONT_HERSHEY_SIMPLEX, 1,(255,255,255),2,cv2.LINE_AA)
            print(ErrorMessage)
            
        if self.colormode!='bgr':
            frame=cv2.cvtColor(frame, self.__ColorModeDict[self.colormode])
    
        return frame
        
    def Start(self):
        """
        Starts the thread to read frames from the video stream
        """
        self.__thread = threading.Thread(target=self.Update, args=())
        self.__thread.setDaemon(True)
        self.__thread.start()
        return self
        
    def Update(self):
        """
        Infinite frame grabber
        """
        while True:
            if self.stopped == True:
                break
            self.frame=self.__Read()
 
    def GetFrame(self):
        """
        Returns the frame most recently read
        """        
        return self.frame
 
    def Stop(self):
        """
        Indicates that the thread should be stopped
        """
        self.stopped = True
        self.__thread.join()
        self.stream.release()
        
###############################################################################       

class Capture:
    """
    Class to capture images or videos with Camera stream
    """
    
    def __init__(self, stream):
        """
        Constructor of Capture
        
        Parameters
        ----------
        stream = stream out of class CameraStream
        
        """
        self.stream = stream
        
        
    def Image(self, img_save=None, img_flip="no"):
        """
        Takes a image of the actual status    
        
        Parameters
        ----------    
        img_save : string with filename to save the image in working directory
        img_flip : flag to flip the image
        
        Returns
        -------
        frame : 2D/3D np.array, int
                Frame containing the image Information
        """
        #Start stream
        self.stream.Start()
        
        #Get frame
        for index in range(10):
            frame = self.stream.GetFrame()
        
        #Stop stream
        self.stream.Stop() 
        
        #Flip image
        if img_flip == "yes":
            frame = np.flipud(frame)
        
        #Save image
        if img_save != None:
            cv2.imwrite(img_save+".png", frame)
            
        return frame
        
    def Video(self, vid_flip="no"):
        """
        Takes and shows a video of the actual status    
        
        Parameters
        ----------    
        vid_flip : flag to flip the image
        
        Returns
        -------
        frame : 2D/3D np.array, int
                Frame containing the image Information
        """
        
        #Start stream
        self.stream.Start()
        
        while True:
            #Get frames
            vid = self.stream.GetFrame()
            
            #Flip video
            if vid_flip == "yes":
                vid = np.flipud(vid)
            
            #Display video
            cv2.imshow('Video',vid)
            
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            
        #Stop stream
        self.stream.Stop() 
        cv2.destroyAllWindows()  
        
       
###############################################################################

class DisplayImage:
    """
    Shows the picture    
    """
    def __init__(self,name,frame):
        """
        """
        self.__frame=cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        self.name=name
        self.Show()
        
    def Show(self):
        """
        """
        self.__figure=plt.figure(self.name)
        plt.imshow(self.__frame)
        plt.show()
         
        
    def Kill(self):
        """
        """
        plt.close(self.__figure)
  